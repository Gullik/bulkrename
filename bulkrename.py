#!/usr/bin/env python3
import glob
import os
import random
import subprocess
import string
import sys
import tempfile

editorDefault = "vi"


def error(message):
    print(f"Error: {message}", file=sys.stderr)
    sys.exit(1)


def warning(message):
    print(f"Warning: {message}", file=sys.stderr)


def generateRandomString(stringLength=32):
    availableCharacters = string.ascii_letters + string.digits
    return "".join((random.choice(availableCharacters) for i in range(stringLength)))


def listContainsDuplicates(l):
    return len(l) != len(set(l))


def mv(source, destination):
    subprocess.check_call(["mv", "-n", source, destination])


def main():
    filenamesOld = sorted(glob.glob("*"))
    nFiles = len(filenamesOld)

    if nFiles == 0:
        error("directory is empty!")
    if listContainsDuplicates(filenamesOld):
        error("duplicate filenames??")

    with tempfile.NamedTemporaryFile() as f:
        for filename in filenamesOld:
            f.write(f"{filename}\n".encode())
        f.flush()

        try:
            editor = os.environ["EDITOR"]
        except KeyError:
            warning(f"EDITOR not set in the environment, defaulting to '{editorDefault}'")
            editor = editorDefault
        subprocess.check_call([editor, f.name])

        f.seek(0)
        filenamesNew = f.read().decode().split("\n")[:-1]
        if len(filenamesNew) != nFiles:
            error("number of lines must not change!")
        if min(filenamesNew) == "":
            error("empty lines are not allowed!")
        if listContainsDuplicates(filenamesNew):
            error("lines cannot be duplicates!")
        if filenamesNew == filenamesOld:
            error("filename list unchanged")

        filenamesTemp = [None] * nFiles
        for i in range(nFiles):
            filenamesTemp[i] = generateRandomString()
        if listContainsDuplicates(filenamesTemp):
            error("list of random strings contains duplicates??")

        for i in range(nFiles):
            mv(filenamesOld[i], filenamesTemp[i])
        for i in range(nFiles):
            mv(filenamesTemp[i], filenamesNew[i])


if __name__ == "__main__":
    main()
