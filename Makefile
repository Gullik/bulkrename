PREFIX = /usr/local

bulkrename:
	@echo "No need to compile me, I am a Python script :)"
	@echo "Install with 'sudo make install'"

clean:
	@:

install:
	install -Dm 755 bulkrename.py "${DESTDIR}${PREFIX}/bin/bulkrename"

uninstall:
	rm -f "${DESTDIR}${PREFIX}/bin/bulkrename"
